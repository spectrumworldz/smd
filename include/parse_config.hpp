#pragma once

#include <string>
#include <vector>
#include "module.hpp"

namespace ns
{

/**
 * @brief Load the modules using the config files
 *
 * @param path Path to the config file
 *
 * @return Loaded modules
 */
std::map<std::string, ns::ModulePtr> parse_config(const std::string& path);
} /* ns */
