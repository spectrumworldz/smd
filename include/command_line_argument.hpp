#pragma once

#include <string>

namespace ns
{

/**
 * @brief Parse and save command line arguments
 *
 * @param argc main argc
 * @param argv main argv
 */
void handle_command_line_arguments(int argc, char** argv);

/**
 * @brief Contains all command line arguments
 */
namespace cla
{
extern std::string server_socket_path;
extern short server_port;
extern std::string config_file_path;
} /* cla */

} /* ns */
