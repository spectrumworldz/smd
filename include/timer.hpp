#pragma once

#include <atomic>
#include <chrono>
#include <functional>
#include <thread>

namespace ns
{

/**
 * @brief Represents a periodic timer.
 */
class Timer
{
public:
    /**
     * @brief Constructor
     *
     * @param callback_function Function to call every time when the timer is
     * expired.
     */
    Timer(std::function<void(void)> callback_function);

    /**
     * @brief Destructor
     *
     * Automatically stops the timer.
     */
    ~Timer();

    /**
     * @brief Starts the periodic timer.
     *
     * @param interval Durations between calls.
     */
    void start(std::chrono::milliseconds interval);

    /**
     * @brief Stops the periodic timer.
     */
    void stop();

    /**
     * @brief Checks the timer is running.
     *
     * @return true if it is running, false otherwise
     */
    bool is_running();

private:
    std::function<void(void)> callback_function_;
    std::thread thread_;
    std::atomic_bool execute_;
};

} /* ns */
