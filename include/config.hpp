#pragma once

#include <iostream>
#include <map>
#include <sstream>
#include <string>

namespace ns
{

/**
 * @brief Represents name-value config pairs.
 */
class Config
{
public:
    Config() = default;

    /**
     * @brief Add a new config.
     *
     * @param name The name of the config
     * @param value The value of the confg
     */
    void set(const std::string& name, const std::string& value);

    /**
     * @brief Checks the specified config is set.
     *
     * @param name The name of the config
     *
     * @return true if set, false otherwise
     */
    bool is_set(const std::string& name) const;

    /**
     * @brief Return the value of config. If not found return the default value.
     *
     * @param name The name of the config.
     * @param default_value Default value of the config
     *
     * @return Return the value of the config or a default value.
     */
    std::string get(const std::string& name,
                    const std::string& default_value) const;

    /**
     * @brief Same as Config::get, but it convert the value of the config to a
     * specific type.
     *
     * @tparam T The type of the config
     * @param name The name of the Config
     * @param default_value Default value
     *
     * @return Returns the value of the config or default value.
     */
    template <typename T>
    T get_as(const std::string& name, const T& default_value) const
    {
        auto it = values_.find(name);
        if (it == values_.end())
        {
            return default_value;
        }
        else
        {
            return convert_to<T>(it->second);
        }
    }

    /**
     * @brief Merge the configs. If a config already represents, it will be
     * overwritten.
     *
     * @param config
     */
    void merge(const Config& config);

    /**
     * @brief Remove all config.
     */
    void clear();

private:
    std::map<std::string, std::string> values_;

    template <typename T>
    T convert_to(const std::string& value) const
    {
        std::stringstream ss(value);

        T tmp;
        ss >> std::boolalpha >> tmp;

        return tmp;
    }
};

} /* ns */
