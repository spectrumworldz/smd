#pragma once

#include <functional>
#include <map>
#include <memory>
#include <mutex>
#include <ostream>
#include <string>

#include "config.hpp"
#include "timer.hpp"

namespace ns
{
class Module;
}
std::ostream& operator<<(std::ostream& os, const ns::Module& module);

namespace ns
{
using ModulePtr = std::shared_ptr<Module>;
using Creator   = std::function<ModulePtr()>;

/**
 * @brief Base class for all modules
 */
class Module
{
public:
    friend std::ostream& ::operator<<(std::ostream& os,
                                      const ns::Module& module);

    virtual ~Module() = default;

    /**
     * @brief
     *
     * @return
     */
    std::string get_value() const;

    /**
     * @brief Sets modules specific config
     *
     * @param config
     *
     * @return
     */
    void set_config(Config& config);

    /**
     * @brief Enable the module. Starts to updating the value.
     */
    void start();

    /**
     * @brief Disable the module. Stops to updating the value.
     */
    void stop();

    /**
     * @brief Returns the update time
     *
     * @return update time
     */
    std::chrono::milliseconds get_update_time() const noexcept;

protected:
    /**
     * @brief Constructor
     */
    Module();

    /**
     * @brief Prints the module values. Child classes can override.
     *
     * @param os stream to print the value. e.g. std::cout
     */
    virtual void print(std::ostream& os) const;

    /**
     * @brief Sets the module specific config values.
     *
     * @param config module specific config values
     */
    virtual void set_config_impl(Config& config) = 0;

    /**
     * @brief Periodicly called function, where the child classes updates the
     * values.
     */
    virtual void update() = 0;

    /**
     * @brief
     *
     * @param value
     */
    void set_value(const std::string& value);

private:
    std::string value_;
    mutable std::mutex mutex_;
    Timer timer_;
    std::chrono::milliseconds update_time_;
    Config config_;
};

/**
 * @brief
 *
 * @tparam T
 *
 * @return
 */
template <typename T>
Creator get_module_creator()
{
    return []() { return std::make_shared<T>(); };
}

/**
 * @brief Register a module
 *
 * @tparam T Type of the module
 * @param name Name of the module
 *
 * @return true if successful, false otherwise
 */
template <typename T>
bool register_module(const std::string& name)
{
    extern std::map<std::string, Creator> registered_modules;
    if (registered_modules.find(name) == registered_modules.end())
    {
        registered_modules[name] = get_module_creator<T>();
        return true;
    }
    return false;
}

/**
 * @brief Create the module using the specific config
 *
 * @param name Name of the module
 * @param config Config for the module
 *
 * @return a new instance for the module
 */
ModulePtr make_module(const std::string& name, Config& config);

} /* ns */
