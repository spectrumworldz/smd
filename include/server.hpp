#pragma once

#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include <cstdio>
#include <iostream>
#include <sstream>

#include "module.hpp"

namespace ns
{

using boost::asio::local::stream_protocol;
using boost::asio::ip::tcp;

/**
 * @brief Sends the modules data using the socket in json format.
 *
 * @tparam Socket Type of socket
 */
template <typename Socket>
class Session : public std::enable_shared_from_this<Session<Socket>>
{
public:
    Session(Socket socket, const std::map<std::string, ns::ModulePtr>& modules)
        : socket_(std::move(socket)), modules_(modules), update_time_(1000)
    {
        // find the lowest update time
        using PairType = std::pair<std::string, ns::ModulePtr>;

        auto it =
            std::min_element(std::begin(modules_), std::end(modules_),
                             [](const PairType& lhs, const PairType& rhs) {
                                 return lhs.second->get_update_time() <
                                        rhs.second->get_update_time();
                             });

        if (it != std::end(modules_))
        {
            update_time_ = it->second->get_update_time();
        }
    }

    void start()
    {
        update_modules_data();

        // send
        boost::asio::async_write(
            socket_, boost::asio::buffer(data_, data_.length()),
            boost::bind(&Session::handle_write, this->shared_from_this(),
                        boost::asio::placeholders::error));
    }

    void handle_write(const boost::system::error_code& error)
    {
        if (!error)
        {
            std::this_thread::sleep_for(update_time_);
            start();
        }
    }

private:
    // The socket used to communicate with the client.
    Socket socket_;

    // Buffer used to send data to the client.
    std::string data_;

    std::map<std::string, ns::ModulePtr> modules_;

    std::chrono::milliseconds update_time_;

    void update_modules_data()
    {
        using boost::property_tree::ptree;

        ptree pt;
        for (auto& m : modules_)
        {
            pt.put(m.first, m.second->get_value());
        }

        std::stringstream ss;
        boost::property_tree::json_parser::write_json(ss, pt);
        data_ = ss.str();
    }
};

/**
 * @brief Represents a unix domain server. Starts listening on the specified
 * path. It creates a new Session object when a new connection received.
 */
class UnixDomainServer
{
public:
    UnixDomainServer(boost::asio::io_service& io_service,
                     const std::string& file,
                     const std::map<std::string, ns::ModulePtr>& modules);

private:
    boost::asio::io_service& io_service_;
    stream_protocol::socket socket_;
    stream_protocol::acceptor acceptor_;
    std::map<std::string, ns::ModulePtr> modules_;

    void do_accept();
};

/**
 * @brief Represents a tcp server. Starts listening on the specified
 * port. It creates a new Session object when a new connection received.
 */
class TcpServer
{
public:
    TcpServer(boost::asio::io_service& io_service, short port,
              const std::map<std::string, ns::ModulePtr>& modules);

private:
    tcp::acceptor acceptor_;
    tcp::socket socket_;
    std::map<std::string, ns::ModulePtr> modules_;

    void do_accept();
};

} /* ns */
