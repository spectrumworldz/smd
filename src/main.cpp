#include "command_line_argument.hpp"
#include "module.hpp"
#include "parse_config.hpp"
#include "server.hpp"

#ifdef SMD_MODULE_NET
#include "interface_statistics.hpp"
#include "ip_address.hpp"
#include "network_speed.hpp"
#endif

#include <iostream>

void register_modules()
{
#ifdef SMD_MODULE_NET
    ns::register_module<ns::InterfaceStatistics>("net_interface_statistics");
    ns::register_module<ns::IpAddress>("net_ip_address");
    ns::register_module<ns::NetworkSpeed>("net_network_speed");
#endif
}

int main(int argc, char** argv)
{
    using namespace ns;

    handle_command_line_arguments(argc, argv);

    register_modules();

    auto modules = parse_config(cla::config_file_path);
    std::cout << "loaded modules: " << modules.size() << std::endl;

    try
    {

        if (cla::server_socket_path.empty())
        {
            // start tcp server
            boost::asio::io_service io_service;
            TcpServer s(io_service, cla::server_port, modules);
            std::cout << "tcp server started: 0.0.0.0:" << cla::server_port
                      << std::endl;
            io_service.run();
        }
        else
        {
            // start unix domain server
            boost::asio::io_service io_service;
            ::unlink(cla::server_socket_path.c_str());
            UnixDomainServer s(io_service, cla::server_socket_path, modules);
            std::cout << "uds server started: " << cla::server_socket_path
                      << std::endl;
            io_service.run();
        }
    }
    catch (std::exception& e)
    {
        std::cerr << "Exception: " << e.what() << "\n";
    }

    return 0;
}
