#include "timer.hpp"

namespace ns
{

Timer::Timer(std::function<void(void)> callback_function)
    : callback_function_(std::move(callback_function)), execute_(false)
{
}

Timer::~Timer() { stop(); }

void Timer::start(std::chrono::milliseconds interval)
{
    if (execute_.load(std::memory_order_acquire))
    {
        stop();
    };

    execute_.store(true, std::memory_order_release);

    thread_ = std::thread([this, interval]() {
        while (execute_.load(std::memory_order_acquire))
        {
            callback_function_();
            std::this_thread::sleep_for(interval);
        }
    });
}

void Timer::stop()
{
    execute_.store(false, std::memory_order_release);

    if (thread_.joinable())
    {
        thread_.join();
    }
}

bool Timer::is_running()
{
    return (execute_.load(std::memory_order_acquire) && thread_.joinable());
}
}
