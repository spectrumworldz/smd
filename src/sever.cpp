#include "server.hpp"

namespace ns
{

UnixDomainServer::UnixDomainServer(
    boost::asio::io_service& io_service, const std::string& file,
    const std::map<std::string, ns::ModulePtr>& modules)
    : io_service_(io_service),
      socket_(io_service),
      acceptor_(io_service, stream_protocol::endpoint(file)),
      modules_(modules)
{
    do_accept();
}

void UnixDomainServer::do_accept()
{
    acceptor_.async_accept(socket_, [this](boost::system::error_code ec) {
        if (!ec)
        {
            std::make_shared<Session<stream_protocol::socket>>(
                std::move(socket_), modules_)
                ->start();
        }

        do_accept();
    });
}

TcpServer::TcpServer(boost::asio::io_service& io_service, short port,
                     const std::map<std::string, ns::ModulePtr>& modules)
    : acceptor_(io_service, tcp::endpoint(tcp::v4(), port)),
      socket_(io_service),
      modules_(modules)
{
    do_accept();
}

void TcpServer::do_accept()
{
    acceptor_.async_accept(socket_, [this](boost::system::error_code ec) {
        if (!ec)
        {
            std::make_shared<Session<tcp::socket>>(std::move(socket_), modules_)
                ->start();
        }

        do_accept();
    });
}
}
