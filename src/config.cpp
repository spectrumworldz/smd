#include "config.hpp"

namespace ns
{

void Config::set(const std::string& name, const std::string& value)
{
    values_[name] = value;
}

bool Config::is_set(const std::string& name) const
{
    return values_.find(name) != values_.end();
}

std::string Config::get(const std::string& name,
                        const std::string& default_value) const
{
    auto it = values_.find(name);
    if (it == values_.end())
    {
        return default_value;
    }
    else
    {
        return it->second;
    }
}

void Config::merge(const Config& config)
{
    std::map<std::string, std::string> new_values = config.values_;
    new_values.insert(values_.begin(), values_.end());
    values_ = std::move(new_values);
}

void Config::clear() { values_.clear(); }
}
