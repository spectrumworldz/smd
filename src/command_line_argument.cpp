#include "command_line_argument.hpp"

#include <boost/program_options.hpp>

#include <iostream>

namespace ns
{

namespace cla
{
std::string server_socket_path;
short server_port = 32000;
std::string config_file_path;
}

void handle_command_line_arguments(int argc, char** argv)
{
    try
    {
        namespace po = boost::program_options;

        // specify command line arguments
        po::options_description desc("Allowed options");
        desc.add_options()("help", "produce help message")(
            "socket", po::value<std::string>(&cla::server_socket_path),
            "path to a unix domain socket")(
            "port", po::value<short>(&cla::server_port)->default_value(32000),
            "network port")("config,c",
                            po::value<std::string>(&cla::config_file_path)
                                ->default_value("/usr/local/etc/smd.conf"),
                            "path to config file");

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        // logic
        if (vm.count("help"))
        {
            std::cout << desc << "\n";
            exit(0);
        }
    }
    catch (std::exception& e)
    {
        std::cerr << "error: " << e.what() << "\n";
    }
    catch (...)
    {
        std::cerr << "Exception of unknown type!\n";
    }
}
}
