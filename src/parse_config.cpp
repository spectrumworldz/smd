#include "parse_config.hpp"

#include <boost/foreach.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

#include <iostream>

namespace ns
{

static ns::Config parse_module_config(boost::property_tree::ptree& config_tree)
{
    ns::Config config;
    BOOST_FOREACH (auto& v, config_tree.get_child(""))
    {
        config.set(v.first.data(), v.second.data());
    }

    return config;
}

static ns::ModulePtr parse_module(boost::property_tree::ptree& module_tree)
{
    ns::Config config = parse_module_config(module_tree.get_child("config"));

    std::string module_name = module_tree.get<std::string>("module");

    return ns::make_module(module_name, config);
}

std::map<std::string, ns::ModulePtr> parse_config(const std::string& path)
{
    std::map<std::string, ns::ModulePtr> loaded_modules;

    try
    {
        std::ifstream file(path.c_str());

        boost::property_tree::ptree root_tree;
        boost::property_tree::read_json(file, root_tree);

        BOOST_FOREACH (auto& v, root_tree.get_child(""))
        {
            std::string module_id = v.first;
            auto module = parse_module(root_tree.get_child(module_id));

            if (module == nullptr)
            {
                std::cout << "cannot parse module: " << module_id << std::endl;
            }
            else
            {
                loaded_modules[module_id] = module;
            }
        }
    }
    catch (std::exception const& e)
    {
        std::cerr << e.what() << std::endl;
    }
    return loaded_modules;
}
}
