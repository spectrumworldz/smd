#include "module.hpp"

#include <iostream>

namespace ns
{

Module::Module() : timer_([this]() { update(); }), update_time_(1000) {}

std::string Module::get_value() const
{
    std::lock_guard<std::mutex> guard{mutex_};
    return value_;
}

void Module::set_config(Config& config)
{
    std::lock_guard<std::mutex> guard{mutex_};
    config_.merge(config);

    int update_time = config.get_as<int>("update_time", 1000);
    update_time_    = std::chrono::milliseconds(update_time);

    set_config_impl(config);
}

void Module::start() { timer_.start(update_time_); }

void Module::stop() { timer_.stop(); }

std::chrono::milliseconds Module::get_update_time() const noexcept
{
    return update_time_;
}

void Module::print(std::ostream& os) const { os << get_value(); }

void Module::set_value(const std::string& value)
{
    std::lock_guard<std::mutex> guard{mutex_};
    value_ = value;
}
}

std::ostream& operator<<(std::ostream& os, const ns::Module& module)
{
    module.print(os);
    return os;
}

namespace ns
{
std::map<std::string, Creator> registered_modules;

ns::ModulePtr make_module(const std::string& name, Config& config)
{
    auto it = registered_modules.find(name);
    if (it == registered_modules.end())
    {
        std::cout << "cannot find module: " << name << std::endl;
        return nullptr;
    }
    else
    {
        auto ptr = it->second();
        ptr->set_config(config);
        ptr->start();
        return ptr;
    }
}
}
