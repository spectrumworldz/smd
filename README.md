# smd

Smd is a server application (tcp or unix domain socket) the keep tracks of specific 
system resources which was specified in a configuration file and sends the data in 
json format. You can list the different modules in the config file. When the application 
starts read the config file, load the modules, and sends the values of the modules as json.

This project is cooperate with the smc project.

## Table of content

## Compiling

The application is module based, so yo can enable or disable different groups if you dont 
want to install the dependecies of the group. The building process is simple using cmake.

1. Download this project
```bash
$ git clone ...
```	

2. Create build directory
```bash
$ cd smd && mkdir build && cd build
```	

3. Compile it.
```bash
$ cmake .. && make
```	
Enable or disable groups:
```bash
$ cmake .. -DSMD_MODULE_NET=off
```	
The format is SMD_MODULE_* where * is the name of the group. See the different groups here.

4. Install it.
```bash
$ sudo make install
```

## Configuration

You have to create a configuration file, where you can list the different modules. 
The application will look this file named as ".smd" in your home directory.
If you want you can specify other path using the -c,--config command line argument.

The format of the configuration file is the following:
```json
{
	"unique_id_for_the_module" : {
		"module": "the_name_of_the_module",
		"config": {
			"the_name_of_the_config_1": "the_value_of_the_config_1",
			"the_name_of_the_config_2": "the_value_of_the_config_2"
		}
	}
}
```	

unique_id_for_the_module: a unique string of the module. This identifies the modules, 
and it will be used in response. The the_name_of_the_module and the the_name_of_the_config_* is 
module specifis values. You can find the specific descriptions on a Groups and Modules chaptest.

## Command line options

| Option  		  | Menaing                                    			 |
| ----------------| ---------------------------------------------------- |
| -h,--help       | Print help message.			               			 |
| -c,--config     | Sets the config file. The default is: $HOME/.smd     |
| -p,--port       | Creates a tcp server on the specified port.          |
| -s,--socket     | Creates a unix domain server on the specified path.  |

## Groups and Modules

### Net group

The net group is collecting network related resources.

Dependencies: -

#### Interface Statistics

Task: Tracks interface statistics provided by the kernel.

Name: net_interface_statistics

Config:

| Name  		        | Value                                                                                                                   |
| ----------------------| ----------------------------------------------------------------------------------------------------------------------- |
| interface_name        | the name of the interface (use ifconfig or ip addr show)            											          |
| statistics_type       | See all possible values here. http://lxr.free-electrons.com/source/Documentation/ABI/testing/sysfs-class-net-statistics |

Example:

```json
{
	"enp5s0_received_bytes": {
		"module": "net_interface_statistics",
		"config": {
			"interface_name": "enp5s0",
			"statistics_type": "rx_bytes"
		}
	},
	"enp5s0_transmitted_bytes": {
		"module": "net_interface_statistics",
		"config": {
			"interface_name": "enp5s0",
			"statistics_type": "tx_bytes"
		}
	}
}
```

## Full example

## Developing
### How to add a new module to an existing group?
### How to add a new module to a new group?
	
