#pragma once

#include <module.hpp>
#include <string>

namespace ns
{

class NetworkSpeed : public Module
{
public:
    NetworkSpeed()          = default;
    virtual ~NetworkSpeed() = default;

protected:
    virtual void set_config_impl(Config& config) override;
    virtual void update() override;

private:
    std::string interface_name_;
    std::string direction_;
    uint64_t prev_value_;

    std::string read();
};

} /* ns */
