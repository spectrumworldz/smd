#pragma once

#include <module.hpp>

#include <fstream>
#include <iomanip>
#include <map>
#include <string>

namespace ns
{

class InterfaceStatistics : public Module
{
public:
    InterfaceStatistics()          = default;
    virtual ~InterfaceStatistics() = default;

protected:
    virtual void set_config_impl(Config& config) override;
    virtual void update() override;

private:
    std::string interface_name_;
    std::string statistics_type_;

    std::string read() const;
};

} /* ns */
