#pragma once

#include <module.hpp>
#include <vector>

namespace ns
{

class IpAddress : public Module
{
public:
    IpAddress()          = default;
    virtual ~IpAddress() = default;

protected:
    virtual void set_config_impl(Config& config) override;
    virtual void update() override;

private:
    std::string interface_name_;
    std::string ip_version_;

    std::string read_ipv4() const;
    std::string read_ipv6() const;

    std::vector<unsigned char> hex_to_bytes(const std::string& hex) const;
};

} /* ns */
