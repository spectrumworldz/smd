#include "network_speed.hpp"
#include <fstream>
#include <sstream>

namespace ns
{

void NetworkSpeed::set_config_impl(Config& config)
{
    interface_name_ = config.get("interface_name", "enp5s0");
    direction_      = config.get("direction", "download");
}

void NetworkSpeed::update() { set_value(read()); }

std::string NetworkSpeed::read()
{
    std::string file_name =
        (direction_ == "download" ? "rx_bytes" : "tx_bytes");
    std::string path = std::string("/sys/class/net/") + interface_name_ +
                       "/statistics/" + file_name;

    // get transmitted bytes
    std::ifstream file(path.c_str());
    uint64_t value = 0;
    file >> value;

    auto diff = value - prev_value_;
    double speed =
        (diff / (double)get_update_time().count()) * 1000;  // Kbytes/s

    prev_value_ = value;

    return std::to_string((uint64_t)speed);
}
}
