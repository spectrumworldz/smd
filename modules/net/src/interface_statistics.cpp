#include "interface_statistics.hpp"
#include <iostream>
#include <sstream>

namespace ns
{

void InterfaceStatistics::set_config_impl(Config& config)
{
    interface_name_  = config.get("interface_name", "enp5s0");
    statistics_type_ = config.get("statistics_type", "rx_bytes");
}

void InterfaceStatistics::update() { set_value(read()); }

std::string InterfaceStatistics::read() const
{
    std::string path = std::string("/sys/class/net/") + interface_name_ +
                       "/statistics/" + statistics_type_;
    std::ifstream file(path.c_str());
    std::string value;
    file >> value;
    return value;
}
}
