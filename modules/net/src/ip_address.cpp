#include "ip_address.hpp"

#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/ip.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>

#include <unistd.h>
#include <cstring>
#include <fstream>
#include <memory>
#include <vector>

namespace ns
{

void IpAddress::set_config_impl(Config& config)
{
    interface_name_ = config.get("interface_name", "enp5s0");
    ip_version_     = config.get("ip_version", "ipv4");
}

void IpAddress::update()
{
    std::string value = "";

    if (ip_version_ == "ipv6")
    {
        value = read_ipv6();
    }
    else
    {
        value = read_ipv4();
    }

    set_value(value);
}

std::string IpAddress::read_ipv4() const
{
    int sock;

    /* Create a socket to be able to use ioctl */
    if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP)) < 0)
    {
        return "";
    }

    struct ifreq ifr = {};
    strcpy(ifr.ifr_name, interface_name_.c_str());
    /* Request the infomation */
    if (ioctl(sock, SIOCGIFADDR, &ifr) < 0)
    {
        close(sock);
        return "";
    }

    struct sockaddr_in* addr_in = (struct sockaddr_in*)&ifr.ifr_addr;
    if (ifr.ifr_addr.sa_family == AF_INET)
    {
        return inet_ntoa(addr_in->sin_addr);
    }

    return "";
}

std::vector<unsigned char> IpAddress::hex_to_bytes(const std::string& hex) const
{
    std::vector<unsigned char> bytes;

    for (unsigned int i = 0; i < hex.length(); i += 2)
    {
        std::string byteString = hex.substr(i, 2);
        unsigned char byte =
            (unsigned char)strtol(byteString.c_str(), NULL, 16);
        bytes.push_back(byte);
    }

    return bytes;
}

std::string IpAddress::read_ipv6() const
{
    std::ifstream file("/proc/net/if_inet6");
    std::string line;
    std::stringstream ss;

    while (std::getline(file, line))
    {
        // reset stream
        ss.str("");
        ss.clear();

        // get the information
        ss << line;
        std::string ip6, interface_name, dummy;
        ss >> ip6 >> dummy >> dummy >> dummy >> dummy >> interface_name;

        if (interface_name != interface_name_)
        {
            continue;
        }

        std::vector<unsigned char> bytes = hex_to_bytes(ip6);

        // convert to ipv6 adress
        char address[INET6_ADDRSTRLEN] = {0};
        if (inet_ntop(AF_INET6, bytes.data(), address, sizeof(address)) == NULL)
        {
            continue;
        }
        return address;
    }

    return "";
}
}
